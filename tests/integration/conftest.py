import dict_tools.data
import pop.hub
import pytest


@pytest.fixture(name="hub")
def integration_hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)

    yield hub

    hub.tool.request.application.APP.freeze()
    del hub.tool.request.application.APP


@pytest.fixture()
def ctx():
    return dict_tools.data.NamespaceDict(acct={})
